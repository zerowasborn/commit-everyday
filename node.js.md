### Middlewares
##### Form submission
- to receive POST values in Express by `body-parser` middleware
- submitted form values will parse to `req.body`
```javascript
// server.js
const bodyParser = require('body-parser')

const middlewares = [
  //...
  bodyParser.urlencoded()
]

// router.js
router.get('/contact', (req, res) => {
  res.render('contact', {
    data: {},
    errors: {}
  })
})

router.post('/contact', (req, res) => {
  res.render('contact', {
    data: req.body, // the body-parser goes here
    errors: {
      message: {
        msg: 'A message is required'
      },
      email: {
        msg: 'That email doesn\'t look right'
      }
    }
  })
})
```

##### Validation & Sanitization
```javascript
// server.js
const validator = require('express-validator')

const middlewares = [
  //...
  validator()
]
```

###### About Validation
```javascript
// router.js
const { check, validationResult } = require('express-validator/check')

router.post('/contact', [
  check('message') // simple validation
    .isLength({ min: 1 })
    .withMessage('Message is required'),
  check('email')
    .isEmail()
    .withMessage('That email doesn\'t look right')
], (req, res) => {
  const errors = validationResult(req)
  res.render('contact', {
    data: req.body,
    errors: errors.mapped()
  })
})
```

###### Sanitization
- can be chained onto the end of validation
- Use `trim()` to remove whitespace from the start and end of the values
- Use `normalizeEmail()` to normalize the email
```javascript
// router.js

const { matchedData } = require('express-validator/filter')

router.post('/contact', [
  check('message')
    .isLength({ min: 1 })
    .withMessage('Message is required')
    .trim(),
  check('email')
    .isEmail()
    .withEmail('That email doesn\'t look right')
    .trim()
    .normalizeEmail()
], (req, res) => {
  const errors = validationResult(req)
  res.render('contact', {
    data: req.body,
    errors: errors.mapped()
  })

  const data = matchedData(req)
  console.log('Sanitized:', data);
})
```
##### Valid Form
- Show flash `express-flash`
- Session helper `express-session`
- Cookie helper `cookie-parser`
```javascript
const cookieParser = require('cookie-parser')
const session = require('express-session')
const flash = require('express-flash')

const middlewares = [
  //...
  cookieParser(),
  session({
    secret: 'super-secret-key',
    key: 'super-secret-cookie',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000 }
  }),
  flash()
]
'super-secret-key'
```
- the `express-flash` middleware adds `req.flash(type, message)` to use in our route handlers
- the `express-flash` middleware adds `messages` to `req.locals`
```javascript
// routes
router.post('/contact', [
  //validation...
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.render('contact', {
      data: req.body,
      errors: errors'contact'
    })
  }

  const data = matchedData(req)
  console.log('Sanitized: ', data)

  req.flash('success', 'Thank for the message! I\'ll be in touch :)')
  res.redirect('/')
})
'success'

// views/index.ejs
<% if (messages.success) { %>
  <div class="flash flash-success"><%= messages.success %></div>
<% } %>

<h1>Working With Forms in Node.js</h1>
```
##### Security considerations
- TLS over HTTPS: always use `TLS` encryption over `https://` when working with forms
to encrypt submitted data
- `http://` sends plain text -> can be visible to anyone to see
- Use `helmet` will add some security from HTTP headers, best to include it right at the top
of my middlewares
```javascript
// server.js
const helmet = require('helmet')

middlewares = [
  helmet()
  //...
]
```

##### Cross-site Request Forgery (CSRF)
- can be protected against this cross-site request forgery by generating a unique token
when user is represented with a form and the validating that token before the POST data is processed
- use `csurf`
```javascript
// server.js
const csrf = require('csurf')

middlewares = [
  // ...
  csrf({ cookie: true })

// routes.js
router.get('/contact', (req, res) => {
  res.render('contact', {
    data: {},
    errors: {},
    csrfToken: req.csrfToken()
  })
})

router.post('/contact', [
  // validations ...
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.render('contact', {
      data: req.body,
      error'contact'.mapped(),
      csrfToken: req.csrfToken()
    })
  }
})

// views/contact.ejs
<form method="post" action="/contact" novalidate>
  <input type="hidden" name="_csrf" value="<%= csrfToken %>">
  // ...
</form>
]
```

- We don't need to modify our POST request handler as all POST requests will now
require a valid token by the `csurf` middleware

##### Cross-site Scripting (XSS)
- always use `<%= %>` in `ejs` to be safe with XSS
- avoid to use `<%- %>` because it outputs a raw string

##### File Uploads
- in HTML, it requires an encoding type of `"multipart/form-data"`
- we can use `multer` to deal with this
```javascript
// routes.js
const multer = require('multer')
const upload = multer({ storage: multer.memoryStorage() })

router.post('/contact', upload.single('photo'), [
  // validation...
], (req, res) => {
  // error handling

  if (req.file) {
    console.log('Uploaded: ', req.file)
  }

  req.flash('success', 'Thanks for the message! I\'ll be in touch :)')
  res.redirect('/')
})

// and the form
```
```html
<form method='post' action='/contact?_csrf=<%= csrfToken %>'
  novalidate enctype='multipart/form-data'>
  <!-- ... -->
</form>
```

##### Populating File Inputs
- In case of validation errors, we can't re-populate file inputs like we did for the text inputs
- So the approach to solve this problem is:
  - uploading the file to a temporary location on the server
  - showing a thumbnail and filename of the attached file
  - adding JavaScript to the form to allow people to remove the selected file or upload a new one
  - moving the file to a permanent location when everything is valid.
