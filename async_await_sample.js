// async function
async function sum(a, b) {
  return a + b
}

// await with Promises
function tripleAfter1Second(number) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(number * 3);
    }, 1000);
  });
}

// when using then
tripleAfter1Second(10).then((result) => {
  console.log(result); // 30
})
// => apply async/await
const finalResult = async function(number) {
  let triple = await tripleAfter1Second(number);
  return triple % 2;
} // error can not take modulus of undefined

const tripleResult = async function(number) {
  try {
    return await tripleAfter1Second(number);
  } catch (error) {
    console.log("Something wrong: ", error);
  }
}
