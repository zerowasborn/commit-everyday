#2018

### 27/3
##### Developing computer software steps*:
- Problem definition
- Requirements development
- Construction planning
- Software architecture, or high-level design
- Detailed design
- Coding and debugging
- Unit testing
- Integration testing
- Integration
- System testing
- Corrective maintenance

### 28/3
- Design is the activity that links requirements to coding and debugging
- "wicked" problem is the one that could be clearly defined only by solving it, or by solving part of it - *Horst Rittel* and *Melvin Webber*.
=> You have to "solve" the problem once in order to clearly define it and then solve it again to create a solution that works.
- Design is sloppy because a good solution is often only subtly different from a poor one.
- Design is also sloppy because it's hard to know when your design is "good enough."
- Human errors can only be avoided if one can avoid the use of humans. Even after the concerns are separated, errors will be made.
- No tool is right for everything

### 2/4
##### Key Note Design Concepts*:
- Software's Primary Technical Imperative is managing complexity. This is greatly aided by a design focus on simplicity.
- Simplicity is achieved in two general ways: minimizing the amount of essential complexity that anyone's brain has to deal with at any one time,
and keeping accidental complexity from proliferating needlessly.
- Design is heuristic. Dogmatic adherence to any single methodology hurts creativity and hurts your programs.
- Good design is iterative; the more design possibilities you try, the better your final design will be.
- Information hiding is a particularly valuable concept. Asking "What should I hide?" settles many difficult design issues.
- Lots of useful, interesting information on design is available outside this book. The perspectives presented here are just the tip of the iceberg.

### 3/4
- @python-numpy: if x is an array of integers, `y = x.astype(float)` is a copy of x with elements converted to floating point. With `x = np.array([1,2,3])`
- @python-trends: According to @pythondevsurvey2017, Django is the most popular framework for Python `41%` developer use it
Numpy, Pandas, and Matplotlib are in close second together `39%`.
- @software-dev:
*The most important skill in software development*
When it comes to writing code, the number one most important skill is how to keep a tangle of features from collapsing under the weight of its own complexity. I’ve worked on large telecommunications systems, console games, blogging software, a bunch of personal tools, and very rarely is there some tricky data structure or algorithm that casts a looming shadow over everything else. But there’s always lots of state to keep track of, rearranging of values, handling special cases, and carefully working out how all the pieces of a system interact. To a great extent the act of coding is one of organization. Refactoring. Simplifying. Figuring out how to remove extraneous manipulations here and there.
- @python-learn @abyteofpython:
  - should avoid using import-star: `from mymodule import *`
  - Use `dir` built-in function to list the identifiers that an object defines.
    with modules: include the functions, classes, and variables defined in that module
  - class variables: fields, class functions: methods
  - class can be inherited, polymorphism, encapsulated, abstract
- @python-principles @zen-of-python:
  - Beautiful is better than ugly.
  - Explicit is better than implicit.
  - Simple is better than complex.
  - Complex is better than complicated.
  - Flat is better than nested.
  - Spare is better than dense.
  - Readability counts.
  - Special cases aren't special enough to break the rules.
  - Although practicality beats purity.
  - Errors should never pass silently.
  - Unless explicitly silenced.
  - In the face of ambiguity, refuse the temptation to guess.
  - There should be one-- and preferably only --obvious way to do it.
  - Although that way may not be obvious at first unless you're Dutch.
  - Now is better than never.
  - Although never is often better than *right* now.
  - If the implementation is hard to explain, it's a bad idea.
  - If the implementation is easy to explain, it may be a good idea.
  - Namespaces are one honking great idea -- let's do more of those!

@keyword
dogmatic adherence: tuân thủ giáo điều
heuristic: dựa trên kinh nghiệm
information hiding concept
aid: hỗ trợ, viện trợ

@django:

```python
# function views:
from django.shortcuts import render
from django.http import HttpResponse, Http404

from .models import Pet

def home(request):
    pets = Pet.objects.all()
    return render(request, 'home.html', {'pets': pets})

def pet_detail(request, id):
    try:
        pet = Pet.objects.get(id=id)
    except Pet.DoesNotExist:
        raise Http404('Pet not found')
    return render(response, 'pet_detail.html', {'pet': pet})
# class views:
from django.views.generic import ListView, DetailView

class PetListView(ListView):
    model = Pet
    template_name = 'home.html'

class PetDetailView(DetailView):
    model = Pet
    template_name = 'pet_detail.html'

# urlpatterns with url
from django.contrib import admin
from django.conf.urls import url

from adoptions import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home, name='home')
    url(r'^adoptions/(\d+)/', views.pet_detail, name='pet_detail')
]
# urlpatterns with path
from django.urls import path
from django.contrib import admin

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.PetListView.as_view(), name='home'),
    path('adoptions/<int:pk>/', views.PetDetailView.as_view(), name='pet_detail'),
]
```
-----------------------------------------------------------------
@node.js @es-2017
### Async & Await
###### Async
- a cleaner way to work with asynchronous code in JavaScript
- Async/Await was created to simplify the process of working with & writing chained promises
- to enhance readability of chained code
```python
async function sum(a, b) {
  return a + b;
}
```
###### Await
- is used to force the rest of the code to wait until that Promise resolves and returns a result
- only works with Promises
- it doesn't work with callbacks
- @await can only be used within a @async function
